from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

# from attendees.models import Attendee
# from events.models import Conference


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                api_url = o.get_api_url()
                d["href"] = api_url

            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


# class ConferenceEncoder(JSONEncoder):
#     def default(self, o):
#         if isinstance(o, Conference):
#             return {
#                 "id": o.id,
#                 "name": o.name,
#                 "starts": o.starts,
#                 "ends": o.ends,
#                 "description": o.description,
#                 # Add more fields as needed
#             }
#         return super().default(o)


# class AttendeeEncoder(JSONEncoder):
#     def default(self, o):
#         if isinstance(o, Attendee):
#             return {
#                 "id": o.id,
#                 "name": o.name,
#                 "email": o.email,
#                 "company_name": o.company_name,
#                 "created": o.created,
#                 "conference": {
#                     "name": o.conference.name,
#                     "href": o.conference.get_api_url(),
#                 },
#             }
#         return super().default(o)

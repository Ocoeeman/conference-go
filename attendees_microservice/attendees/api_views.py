import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO
from django.views.decorators.http import require_http_methods

# from events.models import Conference


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "conference",
    ]
    encoders = {"conference": ConferenceVODetailEncoder()}


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        # "email",
        # "company_name",
        # "conference",
    ]
    # encoders = {"conference": ConferenceVODetailEncoder()}

    # def get_extra_data(self, o):
    #     return {
    #         "created": o.created,
    #         "conference": {
    #             "name": o.conference.name,
    #             "href": o.conference.get_api_url(),
    #         },
    #     }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        # attendees_list = [
        #     AttendeeEncoder().default(attendee) for attendee in attendees
        # ]
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            attendee = Attendee.objects.filter(id=id).update(**content)
            return JsonResponse(
                attendee,
                encoder=AttendeeEncoder,
                safe=False,
            )
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee id"},
                status=400,
            )

        # attendee.name = content.get("name", attendee.name)
        # attendee.email = content.get("email", attendee.email)
        # attendee.company_name = content.get(
        #     "company_name", attendee.company_name
        # )

        attendee.save()

        return JsonResponse(
            attendee,
            encoder=AttendeeEncoder,
            safe=False,
        )

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import pika

from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from events.models import Conference
from .models import Presentation


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


def send_message(queue, body):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(
        exchange="",
        routing_key=queue,
        body=json.dumps(body),
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    body = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    send_message("presentation_approvals", body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    body = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    send_message("presentation_rejections", body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def api_show_presentation(request, pk):
    """
    Returns the details for the Presentation model specified
    by the pk parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


# import json, pika
# from django.http import JsonResponse
# from .models import Presentation
# from common.json import ModelEncoder, ConferenceEncoder
# from django.views.decorators.http import require_http_methods


# @require_http_methods(["GET", "POST"])
# def api_list_presentations(request, conference_id):
#     if request.method == "GET":
#         presentations = [
#             {
#                 "title": p.title,
#                 "status": p.status.name,
#                 "href": p.get_api_url(),
#             }
#             for p in Presentation.objects.filter(conference_id=conference_id)
#         ]
#         return JsonResponse({"presentations": presentations})
#     else:
#         content = json.loads(request.body)

#         # Set the conference ID for the new presentation
#         content["conference_id"] = conference_id

#         # Create a new Presentation object
#         presentation = Presentation.create(**content)

#         return JsonResponse(
#             presentation,
#             encoder=PresentationDetailEncoder,
#             safe=False,
#         )


# class PresentationDetailEncoder(ModelEncoder):
#     model = Presentation
#     properties = [
#         "presenter_name",
#         "company_name",
#         "presenter_email",
#         "title",
#         "synopsis",
#         "created",
#         "conference",
#     ]
#     encoders = {
#         "conference": ConferenceEncoder(),
#     }

#     def get_extra_data(self, o):
#         return {"status": o.status.name}


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_presentation(request, id):
#     if request.method == "GET":
#         try:
#             presentation = Presentation.objects.get(id=id)
#         except Presentation.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid presentation id"},
#                 status=400,
#             )

#         return JsonResponse(
#             presentation,
#             encoder=PresentationDetailEncoder,
#             safe=False,
#         )
#     elif request.method == "DELETE":
#         count, _ = Presentation.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:  # PUT method for updating the presentation
#         content = json.loads(request.body)
#         try:
#             presentation = Presentation.objects.get(id=id)
#         except Presentation.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid presentation id"},
#                 status=400,
#             )

#         # Update the presentation attributes with the provided values
#         presentation.presenter_name = content.get(
#             "presenter_name", presentation.presenter_name
#         )
#         presentation.company_name = content.get(
#             "company_name", presentation.company_name
#         )
#         presentation.presenter_email = content.get(
#             "presenter_email", presentation.presenter_email
#         )
#         presentation.title = content.get("title", presentation.title)
#         presentation.synopsis = content.get("synopsis", presentation.synopsis)

#         presentation.save()

#         return JsonResponse(
#             presentation,
#             encoder=PresentationDetailEncoder,
#             safe=False,
#         )


# @require_http_methods(["PUT"])
# def api_approve_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presentation.approve()
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="tasks")
#     channel.basic_publish(
#         exchange="",
#         routing_key="tasks",
#         body="json.dumps",
#     )
#     connection.close()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# @require_http_methods(["PUT"])
# def api_reject_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presentation.reject()
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )

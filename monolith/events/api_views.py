import json
from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .acls import get_photo, get_weather_data

# this has definitely been tough!


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    # Create a dictionary to hold the conference details
    conference_data = {
        "name": conference.name,
        "starts": conference.starts,
        "ends": conference.ends,
        "description": conference.description,
        "created": conference.created,
        "updated": conference.updated,
        "max_presentations": conference.max_presentations,
        "max_attendees": conference.max_attendees,
        "location": {
            "name": conference.location.name,
            "href": conference.location.get_api_url(),
        }
    }

    # Get the current weather data for the conference's location
    weather_data = get_weather_data(conference.location.city, conference.location.state.abbreviation)

    # Check if weather data is available
    if weather_data is not None:
        conference_data["weather"] = {
            "temp": weather_data["temperature"],
            "description": weather_data["description"]
        }
    else:
        conference_data["weather"] = None

    if request.method == "GET":
        return JsonResponse({"conference": conference_data})

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # PUT method for updating the conference
        content = json.loads(request.body)

        # Update the specific fields with the provided values
        conference.name = content.get("name", conference.name)
        conference.starts = content.get("starts", conference.starts)
        conference.ends = content.get("ends", conference.ends)
        conference.description = content.get("description", conference.description)

        conference.save()

        # Update the conference_data dictionary with the new weather data
        weather_data = get_weather_data(conference.location.city, conference.location.state.abbreviation)
        if weather_data is not None:
            conference_data["weather"] = {
                "temp": weather_data["temperature"],
                "description": weather_data["description"]
            }
        else:
            conference_data["weather"] = None

        return JsonResponse({"conference": conference_data})


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo = get_photo(content["city"], content["state"])
        content.update(photo)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

# import json
# import pika
# import django
# import os
# import sys
# from django.core.mail import send_mail
# from monolith.presentations.models import


import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


def process_message(ch, method, properties, body):
    print("Received %r" % body)


def process_message(ch, method, properties, body, queue):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(
        exchange="",
        routing_key=queue,
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
        ),
    )
    channel.basic_consume(
        queue=queue,
        on_message_callback=process_message,
        auto_ack=False,
    )
    print("Received %r" % body)
    channel.start_consuming()


def approve_presentation(request):
    process_message()

    return JsonResponse({"message": "Presentation "})
